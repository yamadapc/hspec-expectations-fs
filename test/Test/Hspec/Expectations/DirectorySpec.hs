{-# LANGUAGE OverloadedStrings #-}
module Test.Hspec.Expectations.DirectorySpec where

import           Hscaffold
import           Test.Hspec.Expectations.Directory

import           Test.Hspec

spec :: Spec
spec = do
    describe "fileShouldExist" $ do
        it "works" pending

    describe "fileShouldBe" $ do
        it "works" pending

    describe "fileShouldNotBe" $ do
        it "works" pending

    describe "fileShouldSatisfy" $ do
        it "works" pending

    describe "fileShouldNotSatisfy" $ do
        it "works" pending

    describe "fileShouldContain" $ do
        it "works" pending

    describe "fileShouldNotContain" $ do
        it "works" pending

    describe "fileShouldStartWith" $ do
        it "works" pending

    describe "fileShouldEndWith" $ do
        it "works" pending

    describe "fileShouldNotExist" $ do
        it "works" pending

    describe "directoryShouldExist" $ do
        it "works" pending

    describe "directoryShouldNotExist" $ do
        it "works" pending

    describe "directoryShouldMatch" $ do
        it "works with no nesting" $ do
            let s =  do
                    file "something" "here"
                    file "something-else" "else"
            withTemporaryHscaffold s $ \tmp -> do
                s' <- hscaffoldFromDirectory tmp :: IO ScaffoldActionV
                tmp `directoryShouldMatch` s

        it "works with one level of nesting" $ do
            let s =  do
                    file "something" "here"
                    file "something-else" "else"
                    directory "something-else-even" (file "nested" "else")
            withTemporaryHscaffold s $ \tmp -> do
                s' <- hscaffoldFromDirectory tmp :: IO ScaffoldActionV
                tmp `directoryShouldMatch` s

        it "works with 2 levels of nesting" $ do
            let s =  do
                    file "something" "here"
                    file "something-else" "else"
                    directory "something-else-even" $ do
                        file "nested" "else"
                        directory "more-nested" (file "other" "here")
            withTemporaryHscaffold s $ \tmp -> do
                s' <- hscaffoldFromDirectory tmp :: IO ScaffoldActionV
                tmp `directoryShouldMatch` s

        it "works with 3 levels of nesting" $ do
            let s =  do
                    file "something" "here"
                    file "something-else" "else"
                    directory "something-else-even" $ do
                        file "nested" "else"
                        directory "more-nested" $ do
                            file "other" "here"
                            directory "more-nested" $ do
                                file "other" "here"
            withTemporaryHscaffold s $ \tmp -> do
                s' <- hscaffoldFromDirectory tmp :: IO ScaffoldActionV
                tmp `directoryShouldMatch` s
