module Test.Hspec.Expectations.Directory
  where

import           Control.Monad
import           Data.List
import           Data.Monoid
import           Debug.Trace
import           Hscaffold
import           System.Directory
import           Test.Hspec.Expectations

fileShouldExist :: FilePath -> Expectation
fileShouldExist fp = do
    e <- doesFileExist fp
    unless e $
        expectationFailure ("Expected " <> fp <> " to be a file, but it wasn't")

fileShouldBe :: FilePath -> String -> Expectation
fileShouldBe fp txt = do
    fileShouldExist fp
    txt' <- readFile fp
    txt' `shouldBe` txt

fileShouldNotBe :: FilePath -> String -> Expectation
fileShouldNotBe fp txt = do
    fileShouldExist fp
    txt' <- readFile fp
    txt' `shouldNotBe` txt

fileShouldSatisfy :: FilePath -> (String -> Bool) -> IO ()
fileShouldSatisfy fp p = do
    fileShouldExist fp
    txt' <- readFile fp
    txt' `shouldSatisfy` p

fileShouldNotSatisfy :: FilePath -> (String -> Bool) -> IO ()
fileShouldNotSatisfy fp p = do
    fileShouldExist fp
    txt' <- readFile fp
    txt' `shouldNotSatisfy` p

fileShouldContain :: FilePath -> String -> IO ()
fileShouldContain fp cts = do
    fileShouldExist fp
    txt' <- readFile fp
    txt' `shouldContain` cts

fileShouldNotContain :: FilePath -> String -> IO ()
fileShouldNotContain fp cts = do
    fileShouldExist fp
    txt' <- readFile fp
    txt' `shouldNotContain` cts

fileShouldStartWith :: FilePath -> String -> IO ()
fileShouldStartWith fp cts = do
    fileShouldExist fp
    txt' <- readFile fp
    txt' `shouldStartWith` cts

fileShouldEndWith :: FilePath -> String -> IO ()
fileShouldEndWith fp cts = do
    fileShouldExist fp
    txt' <- readFile fp
    txt' `shouldEndWith` cts

fileShouldNotExist :: FilePath -> Expectation
fileShouldNotExist fp = do
    e <- doesFileExist fp
    when e $
        expectationFailure ("Expected " <> fp <> " to *not* be a file, but it was")

directoryShouldExist :: FilePath -> Expectation
directoryShouldExist fp = do
    e <- doesDirectoryExist fp
    unless e $
        expectationFailure ("Expected " <> fp <> " to be a directory, but it wasn't")

directoryShouldNotExist :: FilePath -> Expectation
directoryShouldNotExist fp = do
    e <- doesDirectoryExist fp
    when e $
        expectationFailure ("Expected " <> fp <> " to *not* be a directory, but it was")

directoryShouldMatch :: FilePath -> ScaffoldMonadIO a -> Expectation
directoryShouldMatch fp s = do
    directoryShouldExist fp
    h <- hscaffoldFromDirectory fp
    s' <- execWriterT s
    rsort h `shouldBe` rsort s'

rsort ds = sort (map helper ds)
  where
    helper (Directory f fs) = (Directory f (rsort fs))
    helper x = x
